#!/bin/bash

# Makes gedit stop hiding a new line at the end of a file 
gsettings set org.gnome.gedit.preferences.editor ensure-trailing-newline false
