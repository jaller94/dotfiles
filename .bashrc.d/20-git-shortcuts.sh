#!/bin/bash
# These git commands and aliases should work on all git repos.

# As remote Git branches get deleted their name becomes available for new
# branches. It's a good idea to clean up local branches every now and then.
# This bash function deletes gone remote branches, given that no further local
# changes have been made.
clean_branches () {
    git fetch --all -p
    git branch -vv | grep ": gone" | awk '{print $1}' | xargs -n 1 git branch -D
}
