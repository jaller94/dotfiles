export PATH="$HOME/.bin:$PATH"

export HISTFILESIZE=
export HISTSIZE=
export HISTTIMEFORMAT="%F %T "
# Don't store lines starting with a space or duplicates
export HISTCONTROL=ignoreboth
export HISTIGNORE="ls:bg:fg:history"
shopt -s histappend
# Reduce multiline commands to a single line
shopt -s cmdhist

export VISUAL="atom --wait"
#export VISUAL="code --new-window --wait"
export EDITOR=nano
