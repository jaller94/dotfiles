#!/bin/bash

# Commit w/o doing pre-commit checks
lookMomNoHands() {
  git commit --no-verify -m "$1"
}

# Returns the process that's running on a port
onport() {
  lsof -i :$1
}

# Kills whatever is running on a specified port
killport() {
  kill -9 "$(lsof -i :$1 -t)"
}

emptycommit() {
  git commit --allow-empty -m "$1"
}
