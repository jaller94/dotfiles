#!/bin/bash

rm -r ./.bash_aliases
rm -r ./.bashrc
rm -r ./.bashrc.d
rm -r ./.templates
rm -r ./.config

cp $HOME/.bash_aliases ./.bash_aliases
cp $HOME/.bashrc ./.bashrc
cp -r $HOME/.bashrc.d ./.bashrc.d
cp -r $HOME/.templates ./.templates
mkdir ./.config
cp $HOME/.config/user-dirs.dirs ./.config/
cp -r $HOME/.config/tilda/ ./.config/
cp -r $HOME/.config/xfce4/ ./.config/
