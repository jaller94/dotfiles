#!/bin/bash

alias ..="cd .."
alias ..2="cd ../.."
alias ..3="cd ../../.."
alias ..4="cd ../../../.."
alias ..5="cd ../../../../.."
# Finding the files/directories with recent changes
alias ltra="ls -ltra"
# Finding the largest files/directories on a Linux or Unix-like system
alias ducks='du -cks * | sort -rn | head'
